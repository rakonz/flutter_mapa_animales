import 'package:flutter/material.dart';
import 'package:flutter_animales/widgets/menu_widget.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Perfil"),
        ),
        drawer: MenuWidget(),
          body: Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: size.height * 0.04,
                        horizontal: size.width * 0.14),
                    child: Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 4,
                                color: Colors.grey,
                                offset: Offset(2, 3))
                          ],
                          image: DecorationImage(
                              image: AssetImage("assets/avatar_1.jpg"),
                              fit: BoxFit.cover)),
                    ),
                  ),
                  SizedBox(
                    width: size.width * 0.001,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Jimmy McGill",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text("Amigo del cartel", style: TextStyle(fontSize: 16)),
                      SizedBox(
                        height: 5,
                      ),
                      Text("j.mcgill01@ufromail.cl",
                          style: TextStyle(fontSize: 16)),
                    ],
                  )
                ],
              ),
              Container(
                color: Color(0xffF7F7F7),
                height: 94,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Text("Publicaciones ",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 15)),
                          Text("12")
                        ],
                      ),
                      Column(
                        children: [
                          Text("Puntos sugeridos ",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 15)),
                          Text("2")
                        ],
                      ),
                      Column(
                        children: [
                          Text("Comentarios ",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 15)),
                          Text("34")
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.only(right: size.width*0.6,top: size.width*0.03),
                child: Container(
                  padding: EdgeInsets.only(bottom: 10),
                  child: Text("Publicaciones", style: TextStyle(fontSize: 18,fontWeight: FontWeight.w500),)),
              ),
              Container(
                height: size.height*0.4,
                width: double.infinity,
                child: ListCardPublicaciones(size: size),
              )
            ],
          ),
        ),
    );
  }
}

class ListCardPublicaciones extends StatelessWidget {
  final Size size;

  ListCardPublicaciones({
    required this.size,
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    List<Widget> listacard = [
      CardForo(
        titulo: "a",
        contenido: "a",
        likes: "2",
        comments: "10",
        image: AssetImage("assets/avatar_1.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "a",
        contenido: "a",
        likes: "99",
        comments: "19",
        image: AssetImage("assets/avatar_1.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "a",
        contenido: "b",
        likes: "2",
        comments: "10",
        image: AssetImage("assets/avatar_1.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "a",
        contenido: "b",
        likes: "2",
        comments: "10",
        image: AssetImage("assets/avatar_1.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "a",
        contenido: "b",
        likes: "99",
        comments: "99",
        image: AssetImage("assets/avatar_1.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "a",
        contenido: "b",
        likes: "2",
        comments: "10",
        image: AssetImage("assets/avatar_1.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "a",
        contenido: "a",
        likes: "2",
        comments: "10",
        image: AssetImage("assets/avatar_1.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "a",
        contenido: "a",
        likes: "99",
        comments: "99",
        image: AssetImage("assets/avatar_1.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "a",
        contenido: "a",
        likes: "2",
        comments: "10",
        image: AssetImage("assets/avatar_1.jpg"),
        size: size,
      ),
    ];

    final List<Widget> lista = List.generate(
        listacard.length,
        (index) => Padding(
              padding: EdgeInsets.only(left: 12, bottom: 5),
              child: listacard[index],
            ));

    return SizedBox(
      height: 300,
      child: ListView(
        children: lista,
      ),
    );
  }
}

class CardForo extends StatelessWidget {
  final String titulo;
  final String contenido;
  final String likes;
  final String comments;
  final AssetImage image;

  final Size size;

  CardForo({
    required this.titulo,
    required this.contenido,
    required this.likes,
    required this.comments,
    required this.image,
    required this.size,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: size.width * 0.2, vertical: size.height * 0.02),
      child: Container(
        width: double.infinity,
        height: 110,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                blurRadius: 5,
                color: Colors.black.withOpacity(0.2),
                offset: Offset(0, 5),
              )
            ]),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                  left: size.width * 0.05, top: size.height * 0.02),
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(29),
                    image:
                        DecorationImage(image: this.image, fit: BoxFit.cover)),
              ),
            ),
            SizedBox(
              width: size.width * 0.05,
            ),
            Padding(
              padding: EdgeInsets.only(top: size.height * 0.02),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    this.titulo,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Text(this.contenido),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    child: Container(
                      width: size.width * 0.1,
                      height: 1,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: size.height * 0.02,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.thumb_up_outlined,
                              size: 15,
                            ),
                            Text("\t${this.likes}")
                          ],
                        ),
                        SizedBox(
                          width: size.width * 0.05,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.messenger_outline,
                              size: 15,
                            ),
                            Text("\t${this.comments}")
                          ],
                        ),
                        SizedBox(
                          width: 7,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
