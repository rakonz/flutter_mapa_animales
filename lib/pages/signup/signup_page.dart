import 'package:flutter/material.dart';
import 'package:flutter_animales/blocs/auth_bloc.dart';
import 'package:flutter_animales/pages/home/home_page.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';

class SignUpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authBloc = Provider.of<AuthBloc>(context);
    return Scaffold(
      body: Center(
          child: SingleChildScrollView(
        child: Column(
          children: [
            Text("SignUp"),
            TextButton(
             onPressed: () async {
                final bool isLogin = await authBloc.loginGoogle();
                print(isLogin);
                if (isLogin) {
                  Navigator.pushReplacementNamed(context, 'home');
                } else {
                  print('No se ha podido iniciar sesión');
                }
              },
              child: Text(
                "Sign Up",
                style: TextStyle(color: Colors.white),
              ),
              style: ButtonStyle(
                padding: MaterialStateProperty.all(
                    EdgeInsets.symmetric(horizontal: 40)),
                backgroundColor: MaterialStateProperty.all(Colors.amber),
              ),
            )
          ],
        ),
      )),
    );
  }
}
