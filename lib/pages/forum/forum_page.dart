import 'package:flutter/material.dart';
import 'package:flutter_animales/widgets/card_publicaciones.dart';
import 'package:flutter_animales/widgets/menu_widget.dart';

class ForumPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Center(child: Text("Foro")),
          actions: [
            Padding(
              padding: EdgeInsets.only(right: size.width*0.03),
              child: IconButton(
                onPressed: () => print('Revisar publicaciones y editarlas.'),
                icon: Icon(Icons.mode_edit_outline_outlined)),
            )
          ],
        ),
        drawer: MenuWidget(),
        backgroundColor: Color(0xffF7F7F7),
        body: Padding(
          padding: EdgeInsets.only(top: size.height * 0.03),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [ForumCategories(size: size), ListCardForo(size: size)],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => print('Agregar publicación.'),
          backgroundColor: Colors.white,
          child: Icon(Icons.note_add_outlined, color: Colors.black),
        ),
      ),
    );
  }
}

class ForumCategories extends StatelessWidget {
  const ForumCategories({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: size.width * 0.02),
      child: Container(child: ListBotonFiltro()),
    );
  }
}

class ListBotonFiltro extends StatelessWidget {
  ListBotonFiltro({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> listaBoton = [
      BotonFiltro(
          textColor: Colors.white,
          textButton: "Comunidad",
          backgroundColor: MaterialStateProperty.all(Colors.grey[600]!)),
      BotonFiltro(
          textColor: Colors.black87,
          textButton: "Animales",
          backgroundColor: MaterialStateProperty.all(Colors.grey[200]!)),
      BotonFiltro(
          textColor: Colors.black87,
          textButton: "Presentación",
          backgroundColor: MaterialStateProperty.all(Colors.grey[200]!)),
      BotonFiltro(
          textColor: Colors.black87,
          textButton: "Reunión",
          backgroundColor: MaterialStateProperty.all(Colors.grey[200]!)),
      BotonFiltro(
          textColor: Colors.black87,
          textButton: "Comunidad",
          backgroundColor: MaterialStateProperty.all(Colors.grey[200]!)),
      BotonFiltro(
          textColor: Colors.black87,
          textButton: "Animales",
          backgroundColor: MaterialStateProperty.all(Colors.grey[200]!)),
      BotonFiltro(
          textColor: Colors.black87,
          textButton: "Presentación",
          backgroundColor: MaterialStateProperty.all(Colors.grey[200]!)),
      BotonFiltro(
          textColor: Colors.black87,
          textButton: "Reunión",
          backgroundColor: MaterialStateProperty.all(Colors.grey[200]!)),
    ];

    List<Widget> listaBotones = List.generate(
        listaBoton.length,
        (index) => Padding(
              padding: EdgeInsets.only(
                top: 6,
                bottom: 12,
                right: 14,
              ),
              child: listaBoton[index],
            ));
    return SizedBox(
      height: 55,
      child: ListView(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        children: listaBotones,
      ),
    );
  }
}

class BotonFiltro extends StatelessWidget {
  final String textButton;
  final MaterialStateProperty<Color> backgroundColor;
  final Color textColor;

  BotonFiltro(
      {required this.textColor,
      required this.textButton,
      required this.backgroundColor,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(this.textButton, style: TextStyle(color: textColor)),
      style: ButtonStyle(
          elevation: MaterialStateProperty.all(5),
          shape: MaterialStateProperty.all(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(29))),
          shadowColor: MaterialStateProperty.all(Colors.black),
          backgroundColor: this.backgroundColor),
      onPressed: () {},
    );
  }
}

class ListCardForo extends StatelessWidget {
  final Size size;

  ListCardForo({
    required this.size,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> listacard = [
      CardForo(
        titulo: "¡Nuevo punto de comida!",
        contenido: "Anunciamos un nuevo punto de comida...",
        likes: "2",
        comments: "10",
        image: AssetImage("assets/avatar.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "¡Nuevo asdasdasdasd!",
        contenido: "asdasdasdasd",
        likes: "99",
        comments: "19",
        image: AssetImage("assets/avatar.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "¡Nuevo punto de comida!",
        contenido: "Anunciamos un nuevo punto de comida...",
        likes: "2",
        comments: "10",
        image: AssetImage("assets/avatar.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "¡Nuevo punto de comida!",
        contenido: "Anunciamos un nuevo punto de comida...",
        likes: "2",
        comments: "10",
        image: AssetImage("assets/avatar.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "¡Nuevo asdasdasdasd!",
        contenido: "asdasdasdasd",
        likes: "99",
        comments: "99",
        image: AssetImage("assets/avatar.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "¡Nuevo punto de comida!",
        contenido: "Anunciamos un nuevo punto de comida...",
        likes: "2",
        comments: "10",
        image: AssetImage("assets/avatar.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "¡Nuevo punto de comida!",
        contenido: "Anunciamos un nuevo punto de comida...",
        likes: "2",
        comments: "10",
        image: AssetImage("assets/avatar.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "¡Nuevo asdasdasdasd!",
        contenido: "asdasdasdasd",
        likes: "99",
        comments: "99",
        image: AssetImage("assets/avatar.jpg"),
        size: size,
      ),
      CardForo(
        titulo: "¡Nuevo punto de comida!",
        contenido: "Anunciamos un nuevo punto de comida...",
        likes: "2",
        comments: "10",
        image: AssetImage("assets/avatar.jpg"),
        size: size,
      ),
    ];

    List<Widget> lista = List.generate(
        listacard.length,
        (index) =>
            Padding(padding: EdgeInsets.only(top: 1), child: listacard[index]));
    return Expanded(
      child: ListView(
        physics: BouncingScrollPhysics(),
        children: lista,
      ),
    );
  }
}

