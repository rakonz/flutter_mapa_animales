import 'package:flutter/material.dart';
import 'package:flutter_animales/pages/forum/forum_page.dart';
import 'package:flutter_animales/pages/home/home_map.dart';
import 'package:flutter_animales/pages/profile/profile_page.dart';
import 'package:google_nav_bar/google_nav_bar.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  final List<Widget> _children = [HomeMapPage(), ForumPage(), ProfilePage()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _children.elementAt(_currentIndex),
      ),
      bottomNavigationBar: GNav(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          rippleColor: Colors.black12,
          onTabChange: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
          gap: 8,
          tabBackgroundColor: Colors.grey.withOpacity(0.1),
          color: Colors.grey[700],
          selectedIndex: _currentIndex,
          tabs: [
            GButton(icon: Icons.home, text: "Inicio",),
            GButton(icon: Icons.forum, text: "Foro",),
            GButton(icon: Icons.person, text: "Perfil",),

          ]),
    );
  }

  
}
