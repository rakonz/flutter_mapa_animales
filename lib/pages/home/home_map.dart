import 'package:flutter/material.dart';
import 'package:flutter_animales/pages/favorites/favorite_page.dart';
import 'package:flutter_animales/widgets/menu_widget.dart';

class HomeMapPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Center(child: Text("Mapa")),
          actions: [
            Padding(
              padding: EdgeInsets.only(right: size.width * 0.03),
              child: IconButton(
                icon: Icon(Icons.favorite),
                onPressed: () {
                  Navigator.pushNamed(context,'favorites');
                },
              ),
            )
          ],
        ),
        drawer: MenuWidget(),
        body: Stack(
          children: [
            Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/mapa_puntos.png'),
                  fit: BoxFit.cover
                )
              ),
            ),
            Positioned(
              top: 10,
              left: 10,
              child: FloatingActionButton(
                heroTag: "Search",
                onPressed: () => print('Buscar punto.'),
                child: Icon(Icons.search, color: Colors.black,),
                backgroundColor: Colors.white,
              )
            ),
          ],
          
        ),
        floatingActionButton: FloatingActionButton(
                heroTag: "Add",
                onPressed: () => print('Sugerir punto.'),
                child: Icon(Icons.add, color: Colors.black,),
                backgroundColor: Colors.white,
              )
            ,
      ),
    );
  }
}
