import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animales/blocs/auth_bloc.dart';
import 'package:flutter_animales/pages/favorites/favorite_page.dart';
import 'package:flutter_animales/pages/forum/forum_page.dart';
import 'package:flutter_animales/pages/home/home_page.dart';
import 'package:flutter_animales/pages/profile/profile_page.dart';
import 'package:flutter_animales/pages/signup/signup_page.dart';
import 'package:provider/provider.dart';

import 'utils/constants.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (context) => AuthBloc(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: 'signup',
        routes: {
          'signup': (_) => SignUpPage(),
          'home': (_) => HomePage(),
          'favorites': (_) => FavoritePage(),
          'forum': (_) => ForumPage(),
          'profile': (_) => ProfilePage(),
        },
        theme: ThemeData(
            primaryColor: kPrimaryColor, scaffoldBackgroundColor: Colors.white),
      ),
    );
  }
}
