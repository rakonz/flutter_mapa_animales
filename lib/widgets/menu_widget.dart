import 'package:flutter/material.dart';
import 'package:flutter_animales/pages/settings/settings_page.dart';

class MenuWidget extends StatelessWidget {
  const MenuWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
        elevation: 0,
        child: ListView(padding: EdgeInsets.zero, children: [
          // DrawerHeader(
          //   child: Container(),
          //   decoration: BoxDecoration(
          //       image: DecorationImage(
          //           image: AssetImage("assets/avatar_1.jpg"), fit: BoxFit.cover)),
          // ),
          DrawerHeader(child: Container(height: 200,color: Colors.transparent,)),

          ListTile(
            leading: Icon(
                Icons.map_outlined,
              color: Colors.blue,
            ),
            title: Text("Mapa"),
            onTap: () => Navigator.pushReplacementNamed(context, 'home'),
          ),
          ListTile(
            leading: Icon(
              Icons.settings,
              color: Colors.blue,
            ),
            title: Text("Settings"),
            onTap: () =>
                Navigator.push(context, MaterialPageRoute(builder: (context) {
              return SettingsPage();
            })),
          ),
          ListTile(
            leading: Icon(
              Icons.logout_outlined,
              color: Colors.blue,
            ),
            title: Text("Logout"),
            onTap: () =>
                Navigator.push(context, MaterialPageRoute(builder: (context) {
              return SettingsPage();
            })),
          ),
        ]));
  }
}
