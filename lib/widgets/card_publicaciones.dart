import 'package:flutter/material.dart';

class CardForo extends StatelessWidget {
  final String titulo;
  final String contenido;
  final String likes;
  final String comments;
  final AssetImage image;

  final Size size;

  CardForo({
    required this.titulo,
    required this.contenido,
    required this.likes,
    required this.comments,
    required this.image,
    required this.size,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: size.width * 0.032, vertical: size.height * 0.02),
      child: Container(
        width: double.infinity,
        height: 110,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                blurRadius: 5,
                color: Colors.black.withOpacity(0.2),
                offset: Offset(0, 5),
              )
            ]),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                  left: size.width * 0.05, top: size.height * 0.02),
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(29),
                    image:
                        DecorationImage(image: this.image, fit: BoxFit.cover)),
              ),
            ),
            SizedBox(
              width: size.width * 0.05,
            ),
            Padding(
              padding: EdgeInsets.only(top: size.height * 0.02),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    this.titulo,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Text(this.contenido),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    child: Container(
                      width: size.width * 0.6,
                      height: 1,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: size.height * 0.02,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      children: [
                        Row(
                          children: [
                            Icon(
                              Icons.thumb_up_outlined,
                              size: 15,
                            ),
                            Text("\t${this.likes} like")
                          ],
                        ),
                        SizedBox(
                          width: size.width * 0.2,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.messenger_outline,
                              size: 15,
                            ),
                            Text("\t${this.comments} comments")
                          ],
                        ),
                        SizedBox(
                          width: 7,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
